import React, { useState, useEffect } from 'react';
import { Col, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function Login() {

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	//State to determine whether submit button is enabled or not for conditional rendering
	const [isActive, setIsActive] = useState(true);


	useEffect(() => {
		//Validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password !== '')){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [email, password])


	function authenticate(e) {
		//Prevents page redirection via form submission
		e.preventDefault();

		//Clear input fields
		localStorage.setItem('email', email);

		Swal.fire({
			title: "Login Successful",
			icon: "success",
			text: "You have successfully login"
		})
		setEmail('');
		setPassword('');
	}

	return (       
<Col lg={6}>
	<Form onSubmit={(e) => authenticate(e)}>
		<h1>Login</h1>
		<Form.Group> 
			<Form.Label>Email Adress</Form.Label>
			<Form.Control 
				type="email"
				placeholder="Enter Email"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}
				/>
		</Form.Group>

		<Form.Group> 
			<Form.Label>Password</Form.Label>
			<Form.Control 
				type="password"
				placeholder="Enter Password"
				required
				value={password}
				onChange={e => setPassword(e.target.value)}
				/>
		</Form.Group>
		{isActive ? 
			<Button variant="success" type="submit" className="mt-3">Login</Button>

			:

			<Button variant="success" type="submit" className="mt-3" disabled>Login</Button>

		}
		
	</Form>
   </Col> 
		)
}
