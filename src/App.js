import React from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register'
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import {Container} from 'react-bootstrap';

// For Routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

// The Router {BrowserRouter} component will enable us to simulate page naviation by synchronizing the shown congent nd the show URL i teh web brwoser
function App() {
  return (
    <Router>
      <AppNavbar />
      <Container>
        <Routes>
        < Route path="/" element={ <Home /> }/>
          < Route path="/courses" element={<Courses /> }/>
            < Route path="/login" element={ <Login />} />
            < Route path="/register" element={ <Register />} />
            < Route path="/logout" element={ <Logout />} />
            < Route path="*" element={ <Error />} />
        </Routes>
      </Container>
    </Router>
  );
}

export default App;
